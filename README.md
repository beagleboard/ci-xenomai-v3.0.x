# ci-xenomai-v3.0.x


# Add repo to your apt sources.list

```
deb [trusted=yes] https://beagleboard.beagleboard.io/ci-xenomai-v3.0.x stable main
```

# Quick One line:

```
sudo sh -c "echo 'deb [trusted=yes] https://beagleboard.beagleboard.io/ci-xenomai-v3.0.x stable main' > /etc/apt/sources.list.d/ci-xenomai.list"
```

#

